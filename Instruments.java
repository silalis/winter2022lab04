public class Instruments
{
	private String name;
	private String type;
	private int difficulty;
	
	public void difficultyExplanation(int difficulty)
	{
		switch (difficulty)
		{
			case 5:
				System.out.println("This is an instrument that is not only very hard to start out with but also incredibly hard to master. It will take a lot of time and effort to improve with this one.");
				break;
			case 4:
				System.out.println("This is an instrument that is a little difficult to start out with but does not require very much time to grasp the basics. This instrument will take some time to improve with.");
				break;
			case 3:
			    System.out.println("This is an instrument which is easy to start out with but that quickly becomes more and more difficult as time goes on. Playing basic tunes will be easy but more advanced music will take a lot longer.");
				break;
			case 2:
				System.out.println("This instrument is easy to learn both in the beginner level and intermediary level. Advanced music is however a whole other story. Time will eventually be needed to improve.");
			case 1:
				System.out.println("This instrument is one that is very easy to grasp. Its use is limited so playing anything too advanced is not possible. Maybe you should move on to one which will challenge you more.");
				break;
			default:
				System.out.println("Error");
				break;
		}
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public void setDifficulty(int difficulty)
	{
		this.difficulty = difficulty;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getType()
	{
		return this.type;
	}
	
	public int getDifficulty()
	{
		return this.difficulty;
	}
	
	public Instruments(String name, String type, int difficulty)
	{
		this.name=name;
		this.type=type;
		this.difficulty=difficulty;
	}
}
