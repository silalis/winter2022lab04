import java.util.Scanner;
public class Shop
{
	public static void main (String[] args)
	{
		Scanner reader = new Scanner(System.in);	
		
		Instruments[] products = new Instruments[4];
		for (int i=0; products.length > i; i++)
		{
			System.out.println("Input the name, type and difficulty (scale of 1 to 5) of an instrument:");
			products[i] = new Instruments(reader.next(), reader.next(), reader.nextInt());
		}
		
		System.out.println("Name:" + products[3].getName());
		System.out.println("Type:" + products[3].getType());
		System.out.println("Difficulty:" + products[3].getDifficulty());
		
		System.out.println("Woops...let's update the last input instrument. First the name of the musical instrument:");
		products[products.length-1].setName(reader.next());
		System.out.println("Now the type of this musical instrument:");
		products[products.length-1].setType(reader.next());
		System.out.println("Finally input its difficulty (on a scale of 1 to 5)");
		products[products.length-1].setDifficulty(reader.nextInt());
		
		System.out.println("Name:" + products[3].getName());
		System.out.println("Type:" + products[3].getType());
		System.out.println("Difficulty:" + products[3].getDifficulty());
		
		
		products[3].difficultyExplanation(products[3].getDifficulty());
	}
}
